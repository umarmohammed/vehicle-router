﻿using AutoMapper;
using System;
using VehicleRouter.Core.Entities;
using VehicleRouter.Infrastructure.Vrp.Models;

namespace VehicleRouter.Infrastructure.Infrastructure.Automapper
{
    public class VrpDataProfile : Profile
    {
        public VrpDataProfile()
        {
            CreateMap<VrpData, Scenario>()
                .ForMember(dest => dest.Customers, opt => opt.MapFrom(src => src.GetNodeDemands()))
                .ForMember(dest => dest.Distances, opt => opt.MapFrom(src => src.DistancesMeters));

            CreateMap<(Node node, Demand demand), Customer>()
                .ForMember(dest => dest.Latitude, opt => opt.MapFrom(src => src.node.Latitude))
                .ForMember(dest => dest.Longitude, opt => opt.MapFrom(src => src.node.Longitude))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.node.Name))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.node.Id))
                .ForMember(dest => dest.ServiceDuration, opt => opt.MapFrom(src => TimeSpan.FromSeconds(src.demand.ServiceDurationSeconds)))
                .ForMember(dest => dest.OpenTime, opt => opt.MapFrom(src => DateTime.MinValue + TimeSpan.FromSeconds(src.demand.OpenTimeSeconds)))
                .ForMember(dest => dest.CloseTime, opt => opt.MapFrom(src => DateTime.MinValue + TimeSpan.FromSeconds(src.demand.CloseTimeSeconds)))
                .ForMember(dest => dest.Demand, opt => opt.MapFrom(src => src.demand.DemandValue));
        }
    }
}
