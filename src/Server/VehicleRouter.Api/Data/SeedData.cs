﻿using AutoMapper;
using System.Linq;
using VehicleRouter.Core.Entities;
using VehicleRouter.Infrastructure.Vrp;
using VehicleRouter.Persistence;

namespace VehicleRouter.Api.Data
{
    public static class SeedData
    {
        public static void Seed(this VehicleRouterDbContext db, IMapper mapper)
        {
            var vrp = VrpDataReader.Read("Data/Scenarios/belgium-tw-d2-n50-k10.vrp");

            var scenario = mapper.Map<Scenario>(vrp);   

            SeedScenario(db, scenario);
        }

        private static void SeedScenario(VehicleRouterDbContext db, Scenario scenario)
        {
            if (db.Scenarios.Any()) return;

            db.Scenarios.Add(scenario);

            db.SaveChanges();
        }
    }
}