﻿using System;
using VehicleRouter.Infrastructure.Vrp.Models;

namespace VehicleRouter.Infrastructure.Vrp.Parsers.Kvp
{
    public class VrpKvpParser : IVrpParser
    {
        public VrpKvpParser(Action<VrpData> updateVrpAction)
        {
            UpdateVrpAction = updateVrpAction; 
        }

        public Action<VrpData> UpdateVrpAction { get; }

        public void UpdateVrp(VrpData vrpData)
        {
            UpdateVrpAction(vrpData);
        }
    }
}
