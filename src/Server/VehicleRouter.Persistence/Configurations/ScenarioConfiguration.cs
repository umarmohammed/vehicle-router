﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using VehicleRouter.Core.Entities;

namespace VehicleRouter.Persistence.Configurations
{
    public class ScenarioConfiguration : IEntityTypeConfiguration<Scenario>
    {
        public void Configure(EntityTypeBuilder<Scenario> builder)
        {
            builder.Property(s => s.Distances).HasColumnType("bigint[]");
        }
    }
}
