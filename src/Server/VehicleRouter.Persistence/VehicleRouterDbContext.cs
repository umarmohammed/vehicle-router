﻿using Microsoft.EntityFrameworkCore;
using VehicleRouter.Core.Entities;
using VehicleRouter.Persistence.Configurations;

namespace VehicleRouter.Persistence
{
    public class VehicleRouterDbContext : DbContext
    {
        public VehicleRouterDbContext(DbContextOptions<VehicleRouterDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ScenarioConfiguration());
        }

        public DbSet<Customer> Customers { get; set; } = default!;
        public DbSet<Scenario> Scenarios { get; set; } = default!;
    }
}