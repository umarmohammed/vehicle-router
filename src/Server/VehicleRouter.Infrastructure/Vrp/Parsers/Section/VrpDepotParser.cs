﻿using System.Collections.Generic;
using System.Linq;
using VehicleRouter.Infrastructure.Vrp.Models;

namespace VehicleRouter.Infrastructure.Vrp.Parsers.Section
{
    public class VrpDepotParser : VrpSectionParser
    {
        public VrpDepotParser(int lineNumber, string[] rawLines, string section) : base(lineNumber, rawLines, section)
        {
        }

        protected override void UpdateVrp(VrpData vrpData, IEnumerable<string> sectionLines)
        {
            vrpData.DepotIds = sectionLines
                .Select(line => int.TryParse(line, out var depotId) ? depotId : -1)
                .Where(depotId => depotId > -1)
                .ToList();
        }
    }
}
