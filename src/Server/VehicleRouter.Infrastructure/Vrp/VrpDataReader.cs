﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using VehicleRouter.Infrastructure.Vrp.Models;
using VehicleRouter.Infrastructure.Vrp.Parsers;

namespace VehicleRouter.Infrastructure.Vrp
{
    public class VrpDataReader
    {
        public static VrpData Read(string filename)
        {
            var rawLines = File.ReadAllLines(filename);
            var parsers = rawLines.Select((line, index) => VrpParserFactory.CreateVrpParser(index, rawLines, line));

            var vrpData = new VrpData();
            foreach (var parser in parsers)
            {
                parser.UpdateVrp(vrpData);
            }

            vrpData.DistancesMeters = ComputeDistancesMeters(vrpData.Nodes);

            return vrpData;
        }

        private static long[] ComputeDistancesMeters(List<Node> nodes)
        {
            long R = 6371;  // radius of the earth in km

            var distances = new long[nodes.Count, nodes.Count];
            for (int i = 0; i < nodes.Count; i++)
            {
                for (int j = 0; j < nodes.Count; j++)
                {
                    var x = (nodes[j].Longitude - nodes[i].Longitude) * Math.Cos(0.5 * (nodes[j].Latitude + nodes[i].Latitude));
                    var y = nodes[j].Latitude - nodes[i].Latitude;
                    var d = R * Math.Sqrt(x * x + y * y);
                    distances[i, j] = (long) d * 1000;
                }
            }

            long[] distancesFlat = new long[nodes.Count * nodes.Count];
            Buffer.BlockCopy(distances, 0, distancesFlat, 0, nodes.Count * nodes.Count * sizeof(long));

            return distancesFlat;
        }
    }
}
