﻿using VehicleRouter.Infrastructure.Vrp.Models;

namespace VehicleRouter.Infrastructure.Vrp.Parsers
{
    public class NullParser : IVrpParser
    {
        public void UpdateVrp(VrpData vrpData)
        {
        }
    }
}
