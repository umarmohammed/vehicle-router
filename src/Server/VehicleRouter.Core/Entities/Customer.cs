﻿using System;

namespace VehicleRouter.Core.Entities
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public TimeSpan ServiceDuration { get; set; }
        public DateTime OpenTime { get; set; }
        public DateTime CloseTime { get; set; }
        public double Demand { get; set; }

        public int ScenarioId { get; set; }
    }
}