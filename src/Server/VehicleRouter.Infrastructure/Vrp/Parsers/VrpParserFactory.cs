﻿using VehicleRouter.Infrastructure.Vrp.Models;
using VehicleRouter.Infrastructure.Vrp.Parsers.Kvp;
using VehicleRouter.Infrastructure.Vrp.Parsers.Section;

namespace VehicleRouter.Infrastructure.Vrp.Parsers
{
    public static class VrpParserFactory
    {
        private const string kvpDelimeter = ":";
        private const string sectionString = "_SECTION";

        public static IVrpParser CreateVrpParser(int lineNumber, string[] rawLines, string line) =>
            GetLineType(line) switch
            {
                LineType.Kvp => CreateKvpParser(line),
                LineType.Section => CreateSectionParser(lineNumber, rawLines, line),
                _ => new NullParser()
            };

        private static IVrpParser CreateKvpParser(string line)
        {
            var (key, value) = ParseKvpLine(line);

            return key switch
            {
                "NAME" => (IVrpParser) new VrpKvpParser(v => v.Name = value),
                "DIMENSION" => new VrpKvpParser(v => v.Dimension = int.Parse(value)),
                "CAPACITY" => new VrpKvpParser(v => v.Capacity = double.Parse(value)),
                _ => new NullParser(),
            };
        }

        private static IVrpParser CreateSectionParser(int lineNumber, string[] rawLines, string section) =>
            section switch
            {
                "NODE_COORD_SECTION" => (IVrpParser) new VrpNodeCoordParser(lineNumber, rawLines, section),
                "DEMAND_SECTION" => new VrpDemandParser(lineNumber, rawLines, section),
                "DEPOT_SECTION" => new VrpDepotParser(lineNumber, rawLines, section),
                _ => new NullParser(),
            };

        private static (string key, string value) ParseKvpLine(string line)
        {
            // parse e.g. NAME: belgium-tw-d2-n50-k10
            var parts = line.Split(":");
            return (parts[0], parts[1].Trim());
        }

        private static LineType GetLineType(string line)
        {
            if (line.Contains(kvpDelimeter)) return LineType.Kvp;
            if (line.Contains(sectionString)) return LineType.Section;
            return LineType.Unknown;
        }
    }
}
