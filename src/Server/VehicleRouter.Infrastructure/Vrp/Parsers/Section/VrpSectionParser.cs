﻿using System.Collections.Generic;
using System.Linq;
using VehicleRouter.Infrastructure.Vrp.Models;

namespace VehicleRouter.Infrastructure.Vrp.Parsers.Section
{
    public abstract class VrpSectionParser : IVrpParser
    {
        public int LineNumber { get; }
        public string[] RawLines { get; }
        public string Section { get; }

        public VrpSectionParser(int lineNumber, string[] rawLines, string section)
        {
            LineNumber = lineNumber;
            RawLines = rawLines;
            Section = section;
        }

        public void UpdateVrp(VrpData vrpData)
        {
            UpdateVrp(vrpData, GetSectionLines(vrpData.Dimension));
        }

        protected abstract void UpdateVrp(VrpData vrpData, IEnumerable<string> sectionLines);

        private IEnumerable<string> GetSectionLines(int vrpDimension)
        {
            return RawLines.Skip(LineNumber + 1).Take(vrpDimension);
        }
    }
}
