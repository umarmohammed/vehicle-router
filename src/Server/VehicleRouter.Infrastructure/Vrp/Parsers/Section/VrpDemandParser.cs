﻿using System.Collections.Generic;
using System.Linq;
using VehicleRouter.Infrastructure.Vrp.Models;

namespace VehicleRouter.Infrastructure.Vrp.Parsers.Section
{
    public class VrpDemandParser : VrpSectionParser
    {
        public VrpDemandParser(int lineNumber, string[] rawLines, string section) : base(lineNumber, rawLines, section)
        {
        }

        protected override void UpdateVrp(VrpData vrpData, IEnumerable<string> sectionLines)
        {
            vrpData.Demands = sectionLines.Select(ParseDemandLine).ToList();
        }

        private static Demand ParseDemandLine(string demandLine)
        {
            // parse e.g. 782 0 25200 68400 0
            var parts = demandLine.Split(" ");
            return new Demand
            {
                NodeId = int.Parse(parts[0]),
                DemandValue = double.Parse(parts[1]),
                OpenTimeSeconds = double.Parse(parts[2]),
                CloseTimeSeconds = double.Parse(parts[3]),
                ServiceDurationSeconds = double.Parse(parts[4])
            };
        }
    }
}
