using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using VehicleRouter.Api.Data;

namespace VehicleRouter.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();

            var dbCommands = new DbCommands(args, host);
            dbCommands.Process();

            host.Run();
        }

        public static IHostBuilder CreateWebHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}